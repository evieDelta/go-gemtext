package gemtext

import (
	"strings"
	"unicode"
)

// BasicTypes returns a slice containing only the core line types of gemtext
// it is safe to append to the returned slice to add custom types
func BasicTypes() []LineParser {
	return []LineParser{
		Link{},
		PreformattedText{},
	}
}

// AllTypes returns a slice containing all the standard gemtext linetypes
// it is safe to append to the returned slice to add custom types
func AllTypes() []LineParser {
	return []LineParser{
		Link{},
		PreformattedText{},
		Heading1,
		Heading2,
		Heading3,
		UnorderedList{},
		Quote{},
	}
}

// ParseMeta is the data given to line parsers
type ParseMeta struct {
	Text string

	// only used for multiline structures
	// tells you if there is a new line right after the prefix
	// for example with preformatted text
	//     ```
	//     example
	//     ```
	// would put this as false, there is only a newline after the prefix
	// while
	//     ```alt
	//     example
	//     ```
	// would put this as true, there is text on the prefix line
	TextOnPrefixLine bool
}

// LineParser is the interface used to define custom line types
type LineParser interface {
	Config() LineConfig
	Parse(ParseMeta) (Line, error)
}

// LineConfig contains some settings for how the parser will treat LineParsers
type LineConfig struct {
	CustomKey string // the identifier used by a custom type to identify itself to a formatter (must be unique)

	// the prefix that identifies a formatting structure (eg: => for links)
	// this is required unless you wish to override how text lines are parsed
	Prefix string
	// a suffix is required for multiline mode unless you wish for an element to extend to the end of text
	// not neecessary outside of multiline mode, as single line mode will always end at a new line
	// but it'll trim the suffix of in single line mode too
	Suffix string
	// TrimWhitespace causes it to trim and leading and trailing whitespace before calling the parse method
	// unless you need the whitespace or wish to trim it out yourself its recommended to just leave this on
	TrimWhitespace bool
	// multiline mode allows it to parse formatting that cover multiple lines
	// (such as preformatted text that starts and ends with ```)
	Multiline bool
	// only applicable in multiline mode
	// allows determining where it'll consider a valid suffix for multiline elements
	// default is BeforeNewline
	MultilineSuffixMode MultilineSuffixMode

	// if there is a newline right after the prefix trim it?
	// this is here as an alternative to trim whitespace
	// MultilineTrimNewlineAfterPrefix bool
}

//go:generate stringer -type=MultilineSuffixMode -linecomment

type MultilineSuffixMode uint8

// modes for how to treat the suffix of multiline elements
const (
	multilineUnspecified MultilineSuffixMode = 0 // unspecified/default

	// require a suffix to exclusively on its own with a line
	// (assuming a suffix of :> it would require the text to be "...\n:>" followed by either /n or eof in order to count)
	MultilineSuffixExclusive MultilineSuffixMode = 1 // exclusive line

	// require the suffix to appear before a newline or eof in order to count
	// (assuming a suffix of :> "text:>\n" would be valid, but "text:>text" would not)
	MultilineSuffixBeforeNewline MultilineSuffixMode = 2 // before newline

	// allows the suffix to appear anywhere in text and not just when followed by a newline
	// anything that follows the suffix with this on will be treated as a newline even if its not
	// (assuming a suffix of :> and an input of `...eot:>moretext` moretext will be treated as if its on a new line)
	// if the suffix is followed by a newline it will still trim that however
	// (to avoid inserting a blank line that doesn't exist into the ast)
	MultilineSuffixAnywhere MultilineSuffixMode = 3 // anywhere

	// Default is BeforeNewline, see the docs on that for more
	MultilineSuffixDefault = MultilineSuffixBeforeNewline
)

type Link struct{}

func (Link) Config() LineConfig {
	return LineConfig{
		Prefix:         "=>",
		TrimWhitespace: true,
	}
}

func (Link) Parse(m ParseMeta) (Line, error) {
	s := m.Text
	i := strings.IndexFunc(s, unicode.IsSpace)
	if i == -1 {
		i = len(s) - 1
	}
	link, text := s[:i], s[i+1:]
	if text == "" {
		text = link
	}

	return Line{
		Type: LineTypeLink,
		Text: text,
		Meta: map[MetaType]string{
			LinkRef: link,
		},
	}, nil
}

const (
	LinkRef = MetaLink
)

type PreformattedText struct{}

func (PreformattedText) Config() LineConfig {
	return LineConfig{
		Prefix:              "```",
		Suffix:              "```",
		Multiline:           true,
		MultilineSuffixMode: MultilineSuffixDefault,
	}
}

func (PreformattedText) Parse(m ParseMeta) (Line, error) {
	var txt, alt string

	if m.TextOnPrefixLine {
		p := strings.SplitN(m.Text, "\n", 2)
		alt = p[0]
		txt = p[1]
		alt = strings.TrimSuffix(alt, "\r") // bleh windows
	} else {
		txt = m.Text
	}

	return Line{
		Type: LineTypePreformattedText,
		Text: txt,
		Meta: map[MetaType]string{
			PreformattedTextAlttext: alt,
		},
	}, nil
}

const (
	PreformattedTextAlttext = MetaAlttext
)

type heading uint8

func (h heading) Config() LineConfig {
	return LineConfig{
		Prefix:         strings.Repeat("#", int(h)),
		TrimWhitespace: true,
	}
}

func (h heading) Parse(m ParseMeta) (Line, error) {
	heading := LineType1stHeading + LineType(h-1)

	return Line{
		Type: heading,
		Text: m.Text,
	}, nil
}

// Headings
const (
	Heading1 heading = 1
	Heading2 heading = 2
	Heading3 heading = 3
)

type UnorderedList struct{}

func (UnorderedList) Config() LineConfig {
	return LineConfig{
		Prefix:         "* ",
		TrimWhitespace: true,
	}
}

func (UnorderedList) Parse(m ParseMeta) (Line, error) {
	return Line{
		Type: LineTypeUnorderedList,
		Text: m.Text,
	}, nil
}

type Quote struct{}

func (Quote) Config() LineConfig {
	return LineConfig{
		Prefix:         ">",
		TrimWhitespace: true,
	}
}

func (Quote) Parse(m ParseMeta) (Line, error) {
	return Line{
		Type: LineTypeQuote,
		Text: m.Text,
	}, nil
}
