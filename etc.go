package gemtext

//go:generate stringer -type=LineType -linecomment
//go:generate stringer -type=MetaType -linecomment

type LineType uint

// LineTypes for the AST
const (
	LineTypeInvalid          LineType = iota
	LineTypeText                      // text
	LineTypeLink                      // link
	LineTypePreformattedText          // preformatted text
	LineType1stHeading                // 1st heading
	LineType2ndHeading                // 2nd heading
	LineType3rdHeading                // 3rd heading
	LineTypeUnorderedList             // unordered list
	LineTypeQuote                     // quoteblock
	LineTypeCustom                    // custom
)

type Formatter interface {
	Format(LineReader) []byte
}

type Line struct {
	Type LineType            // the type of formatting for this block
	Line int                 // the line this block is found on
	Meta map[MetaType]string // meta values

	Text   string // the basic text that should be shown to a user
	Source string // the source text it was generated from

	// only used if LineType == custom
	// functions as the identifier for custom formatting types
	CustomID string
}

// MetaType is the key of a meta value
// there are the primary meta values, as consts prefixed with Meta
// and each builtin line type also has its own aliases of the keys they use
//
// values 100 and below are reserved for internal use
// values above 100 can be used by custom linetypes
type MetaType uint

// MetaTypes
const (
	MetaLink    MetaType = iota // link
	MetaAlttext                 // alttext
)
