package gemtext

// LineReader is a wrapper around a slice of lines for easy reading
// of the abstract syntax array
// it is safe to copy as it is assumed Lines will not change after creation
type LineReader struct {
	Lines []Line
	Index int
}

// Continue returns true if there are more lines to read
func (r *LineReader) Continue() bool {
	return r.Index < len(r.Lines)
}

// Step returns the current line and then increments the pointer by 1
func (r *LineReader) Step() (l Line) {
	l = r.Current()
	r.Index++
	return
}

// Reset the pointer to 0
func (r *LineReader) Reset() {
	r.Index = 0
}

// Current returns the current line
func (r *LineReader) Current() Line {
	return r.Lines[r.Index]
}

// Peek returns the line + or - i
// it returns a line with type Invalid if the requested line is out of bounds
func (r *LineReader) Peek(i int) Line {
	p := r.Index + i
	if p < 0 || p >= len(r.Lines) {
		return Line{}
	}
	return r.Lines[p]
}

// PeekBack is an alias to Peek(-1)
func (r *LineReader) PeekBack() Line {
	return r.Peek(-1)
}

// PeekAhead is an alias to Peek(1)
func (r *LineReader) PeekAhead() Line {
	return r.Peek(1)
}
