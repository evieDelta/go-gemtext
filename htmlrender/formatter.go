package htmlrender

import (
	"bytes"
	"fmt"
	"html"
	"io"

	"codeberg.org/eviedelta/go-gemtext"
)

func NewFormatter(fc FormatterConf) (*Formatter, error) {
	//	if fc.Parser == nil {
	//		fc.Parser = gemtext.DefaultParser
	//	}
	if fc.BuiltinTypes == nil {
		fc.BuiltinTypes = make(map[gemtext.LineType]Custom)

		fc.BuiltinTypes[gemtext.LineTypeText] = LineText
		fc.BuiltinTypes[gemtext.LineTypeLink] = LineLink
		fc.BuiltinTypes[gemtext.LineTypePreformattedText] = LinePreformattedText
		fc.BuiltinTypes[gemtext.LineType1stHeading] = LineHeading1
		fc.BuiltinTypes[gemtext.LineType2ndHeading] = LineHeading2
		fc.BuiltinTypes[gemtext.LineType3rdHeading] = LineHeading3
		fc.BuiltinTypes[gemtext.LineTypeUnorderedList] = LineUnorderedList
		fc.BuiltinTypes[gemtext.LineTypeQuote] = LineQuote
	}
	if fc.CustomTypes == nil {
		fc.CustomTypes = make(map[string]Custom)
	}

	/*
		if fc.BuiltinTypes[gemtext.LineTypeText] == nil {
			fc.BuiltinTypes[gemtext.LineTypeText] = LineText
		}
		if fc.BuiltinTypes[gemtext.LineTypeLink] == nil {
			fc.BuiltinTypes[gemtext.LineTypeLink] = LineLink
		}
		if fc.BuiltinTypes[gemtext.LineTypePreformattedText] == nil {
			fc.BuiltinTypes[gemtext.LineTypePreformattedText] = LinePreformattedText
		}
		if fc.BuiltinTypes[gemtext.LineType1stHeading] == nil {
			fc.BuiltinTypes[gemtext.LineType1stHeading] = LineHeading1
		}
		if fc.BuiltinTypes[gemtext.LineType2ndHeading] == nil {
			fc.BuiltinTypes[gemtext.LineType2ndHeading] = LineHeading2
		}
		if fc.BuiltinTypes[gemtext.LineType3rdHeading] == nil {
			fc.BuiltinTypes[gemtext.LineType3rdHeading] = LineHeading3
		}
		if fc.BuiltinTypes[gemtext.LineTypeUnorderedList] == nil {
			fc.BuiltinTypes[gemtext.LineTypeUnorderedList] = LineUnorderedList
		}
		if fc.BuiltinTypes[gemtext.LineTypeQuote] == nil {
			fc.BuiltinTypes[gemtext.LineTypeQuote] = LineQuote
		}
	*/

	f := Formatter{
		conf: fc,
	}

	return &f, nil
}

type Formatter struct {
	conf FormatterConf
}

type FormatterConf struct {
	//Parser *gemtext.Parser

	CustomTypes  map[string]Custom
	BuiltinTypes map[gemtext.LineType]Custom
}

// FormatString writes a parsed gemtext file into html and returns a string
// See FormatWriter for more documentation
func (f *Formatter) FormatString(r gemtext.LineReader) (string, error) {
	w := &bytes.Buffer{}
	err := f.FormatWriter(r, w)

	return w.String(), err
}

// FormatWriter writes a parsed gemtext file into html
// the html header, footer, and stylesheets is left up to the caller to handle
func (f *Formatter) FormatWriter(r gemtext.LineReader, w io.Writer) error {
	for r.Continue() {
		err := f.handleLine(w, &r)
		if err != nil {
			return err
		}
		r.Step()
	}

	return nil
}

func (f *Formatter) handleLine(w io.Writer, r *gemtext.LineReader) error {
	l := r.Current()
	if l.Type == gemtext.LineTypeCustom || f.conf.BuiltinTypes[l.Type] == nil {
		return f.handleCustom(w, l, r)
	}

	err := f.conf.BuiltinTypes[l.Type].Write(w, l, r)
	if err != nil {
		switch b := err.(type) {
		default:
			err = gemtext.NewError(l.Line, b, "unspecified formatter error (c)")
		case gemtext.ErrorLineModify:
			err = b.ShiftLineNumber(l.Line)
		case gemtext.ErrorLine:
			// do nothing in this case
		}
		return err
	}
	return err
}

func (f *Formatter) handleCustom(w io.Writer, l gemtext.Line, r LineReader) (err error) {
	p, ok := f.conf.CustomTypes[l.CustomID]
	if !ok && l.Text != "" {
		_, err = fmt.Fprintf(w, "<del class='%v'>?(no formatter) <pre>%v</pre></del>\n", CSSClassUnknownCustom, html.EscapeString(l.Text))
		if err != nil {
			err = gemtext.NewError(l.Line, err, "write error")
		}
	} else if !ok {
		_, err = fmt.Fprintf(w, "<del class='%v'>?(no formatter) <pre>%v</pre></del>\n", CSSClassUnknownCustom, html.EscapeString(l.Source))
		if err != nil {
			err = gemtext.NewError(l.Line, err, "write error")
		}
	} else {
		err = p.Write(w, l, r)
		if err != nil {
			switch b := err.(type) {
			default:
				err = gemtext.NewError(l.Line, b, "unspecified formatter error (c)")
			case gemtext.ErrorLineModify:
				err = b.ShiftLineNumber(l.Line)
			case gemtext.ErrorLine:
				// do nothing in this case
			}
			return err
		}
	}
	return err
}

// Custom defines a custom unit
type Custom interface {
	Write(w io.Writer, l gemtext.Line, r LineReader) error
}

// LineReader is a linereader but somewhat more restricted
type LineReader interface {
	Current() gemtext.Line
	Peek(i int) gemtext.Line
	PeekBack() gemtext.Line
	PeekAhead() gemtext.Line
}
