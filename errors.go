package gemtext

import (
	"strconv"
)

// ErrorLine is an error interface to report on which line an error occoured
// for use with custom multiline elements that can report an error, in either
// the parser or the formatter (only useful in multiline elements.)
//
// recommended practice is to report the line as releative to the start of the
// element source text, and also implement ErrorLineModify so the parser or
// formatter can add the global line number to it
type ErrorLine interface {
	LineNumber() int
}

// ErrorLineModify is used to modify the line number reported by ErrorLine
// as to modify a relative line number to be the global line number
// if this is not supported, giving the absolute line number is recommended
type ErrorLineModify interface {
	ShiftLineNumber(by int) error
}

// ErrorUserMessage is used to be able to attatch a user visible error message
// to an error. anything reported by this method may be shown to an end user.
// the output of this should not include the line number, that should be left
// to ErrorLine and whichever device happens to display the error to a user
type ErrorUserMessage interface {
	UserErrorMessage() string
}

// NewError returns a new error
func NewError(line int, err error, usermsg string) error {
	return &Error{
		Line:    line,
		UserMsg: usermsg,
		Err:     err,
	}
}

// UnfoldError returns the fields of an error
func UnfoldError(e error) (ok bool, err Error) {
	b, ok := e.(Error)
	if !ok {
		return false, b
	}

	return true, b
}

// Error is an error with added metadata for a User focused error message and
// which the line the error happened at
type Error struct {
	Line    int
	UserMsg string
	Err     error
}

func (e Error) Error() string {
	return strconv.Itoa(e.Line) + ": " + e.Err.Error()
}

func (e Error) LineNumber() int {
	return e.Line
}

func (e *Error) ShiftLineNumber(by int) error {
	e.Line += by
	return e
}

func (e Error) UserErrorMessage() string {
	return e.UserMsg
}

// Unwrap gives the error located in e
func (e Error) Unwrap() error {
	return e.Err
}

// Cause gives the error located in e
func (e Error) Cause() error {
	return e.Err
}

func UnfoldLineWrappedError(e error) (ok bool, line int, err error) {
	b, ok := e.(WrappedErrorWLine)
	if !ok {
		return false, 0, e
	}

	return true, b.Line, b.error
}

// WrappedErrorWLine is an error that contains an error and a line number
// if a custom formatting element or custom element formatter returns an error
// that does not have a line number, the formatter/parser will wrap the error
// in one of these types giving it a line number
// it implements the ErrorLine interface, and to unfold the contained error
//
type WrappedErrorWLine struct {
	error
	Line int
}

func (e WrappedErrorWLine) Error() string {
	return strconv.Itoa(e.Line) + ": " + e.error.Error()
}

// Unwrap gives the error located in e
func (e WrappedErrorWLine) Unwrap() error {
	return e.error
}

// Cause gives the error located in e
func (e WrappedErrorWLine) Cause() error {
	return e.error
}

func (e WrappedErrorWLine) LineNumber() int {
	return e.Line
}

func (e *WrappedErrorWLine) ShiftLineNumber(by int) {
	e.Line += by
}
