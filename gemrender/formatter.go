package gemrender

import (
	"bytes"
	"io"

	"codeberg.org/eviedelta/go-gemtext"
	"emperror.dev/errors"
)

// NewFormatter returns a new gemtext renderer
//
// probably sounds a bit pointless to have a gemtext > gemtext renderer
// but its not quite as pointless as it sounds when used with custom line types
func NewFormatter(fc FormatterConf) (*Formatter, error) {
	//if fc.Parser == nil {
	//	fc.Parser = gemtext.DefaultParser
	//}
	if fc.BuiltinTypes == nil {
		fc.BuiltinTypes = make(map[gemtext.LineType]Custom)
	}
	if fc.CustomTypes == nil {
		fc.CustomTypes = make(map[string]Custom)
	}

	f := Formatter{
		conf: fc,
	}

	return &f, nil
}

type Formatter struct {
	conf FormatterConf
}

type FormatterConf struct {
	//Parser *gemtext.Parser

	CustomTypes  map[string]Custom
	BuiltinTypes map[gemtext.LineType]Custom
}

// FormatString writes a parsed file back into gemtext returning a string
// See FormatWriter for more documentation
func (f *Formatter) FormatString(r gemtext.LineReader) (string, error) {
	w := &bytes.Buffer{}
	err := f.FormatWriter(r, w)

	return w.String(), err
}

// FormatWriter writes a parsed file back into gemtext
// the html header, footer, and stylesheets is left up to the caller to handle
func (f *Formatter) FormatWriter(r gemtext.LineReader, w io.Writer) error {
	for r.Continue() {
		err := f.handleLine(w, &r)
		if err != nil {
			return err
		}
		r.Step()
	}

	return nil
}

func (f *Formatter) handleLine(w io.Writer, r *gemtext.LineReader) (err error) {
	l := r.Current()
	if l.Type == gemtext.LineTypeCustom {
		return f.handleLineCustom(w, l, r)
	}

	p, ok := f.conf.BuiltinTypes[l.Type]
	if !ok {
		_, e1 := w.Write([]byte(l.Source))
		_, e2 := w.Write([]byte{'\n'})
		err = errors.Append(e1, e2)
		if err != nil {
			return gemtext.NewError(l.Line, err, "write error (b)")
		}
		return
	}

	err = p.Write(w, l, r)
	if err != nil {
		switch b := err.(type) {
		default:
			err = gemtext.NewError(l.Line, b, "unspecified formatter error (b)")
		case gemtext.ErrorLineModify:
			err = b.ShiftLineNumber(l.Line)
		case gemtext.ErrorLine:
			// do nothing in this case
		}
		return err
	}

	return
}

func (f *Formatter) handleLineCustom(w io.Writer, l gemtext.Line, r LineReader) (err error) {
	p, ok := f.conf.CustomTypes[l.CustomID]
	if !ok {
		_, e1 := w.Write([]byte(l.Source))
		_, e2 := w.Write([]byte{'\n'})
		err = errors.Append(e1, e2)
		if err != nil {
			return gemtext.NewError(l.Line, err, "write error (c)")
		}
		return
	}
	err = p.Write(w, l, r)
	if err != nil {
		switch b := err.(type) {
		default:
			err = gemtext.NewError(l.Line, b, "unspecified formatter error (c)")
		case gemtext.ErrorLineModify:
			err = b.ShiftLineNumber(l.Line)
		case gemtext.ErrorLine:
			// do nothing in this case
		}
		return err
	}
	return nil
}

// Custom defines a custom unit
type Custom interface {
	Write(w io.Writer, l gemtext.Line, r LineReader) error
}

// LineReader is a linereader but somewhat more restricted
type LineReader interface {
	Current() gemtext.Line
	Peek(i int) gemtext.Line
	PeekBack() gemtext.Line
	PeekAhead() gemtext.Line
}
