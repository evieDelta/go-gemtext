package htmlrender

import (
	"fmt"
	"html"
	"io"

	"codeberg.org/eviedelta/go-gemtext"
	"emperror.dev/errors"
)

// HTMLElement writes an html element from a gemtext block with some metadata
type HTMLElement struct {
	// the html tags used (in order of highest to lowest)
	Tags []string

	// the css class given to an instance of this
	// (only given to the top level tag if more than 1 is used)
	Class string

	// whether or not it will merge the tags with the next and previous tags
	JoinAdjacent bool

	// AdjacentTags are html takes like the ones in tags but are repeated
	// for every line regardless of JoinAdjactent
	AlwaysTags []string

	// print a linebreak before and after the line content?
	Linebreak bool

	// suffix the result with this
	Suffix string
	// prefix the result with this
	Prefix string

	// a small function that decides how to write the other meta tags
	// such as for links
	MetaWriter func(tag string, metas map[gemtext.MetaType]string) string
}

func (h HTMLElement) Write(w io.Writer, l gemtext.Line, r LineReader) (err error) {
	// if we are joining adjacent and the type of the preceeding block lines up
	// we can skip writing the leading html tags this round
	_, err = io.WriteString(w, h.Prefix)
	defer func() {
		_, er := io.WriteString(w, h.Suffix+"\n")
		err = errors.Append(err, er)
	}()

	if b := r.PeekBack(); !h.JoinAdjacent || b.Type != l.Type || b.CustomID != l.CustomID {
		// write the first tag with the html class
		additions := ""
		if h.MetaWriter != nil {
			additions = h.MetaWriter(h.Tags[0], l.Meta)
		}

		_, er := fmt.Fprintf(w, "<%v class='%v' %v>", h.Tags[0], h.Class, additions)
		err = errors.Append(err, er)

		for _, x := range h.Tags[1:] {
			additions := ""
			if h.MetaWriter != nil {
				additions = h.MetaWriter(x, l.Meta)
			}

			_, er := fmt.Fprintf(w, "<%v %v>", x, additions)
			err = errors.Append(err, er)
		}
	}
	for _, x := range h.AlwaysTags {
		additions := ""
		if h.MetaWriter != nil {
			additions = h.MetaWriter(x, l.Meta)
		}

		_, er := fmt.Fprintf(w, "<%v %v>", x, additions)
		err = errors.Append(err, er)
	}

	// write the actual text
	if h.Linebreak {
		_, er := fmt.Fprintf(w, "\n%v", html.EscapeString(l.Text))
		err = errors.Append(err, er)
	} else {
		_, er := io.WriteString(w, html.EscapeString(l.Text)) // no html injection for you
		err = errors.Append(err, er)
	}

	// write leading html tags
	for i := len(h.AlwaysTags) - 1; i >= 0; i-- {
		_, er := fmt.Fprintf(w, "</%v>", h.AlwaysTags[0])
		err = errors.Append(err, er)
	}

	// if we are joining adjacent and the type of the following block lines up
	// we can skip writing the trailing html tags this round
	if b := r.PeekAhead(); h.JoinAdjacent && b.Type == l.Type && b.CustomID == l.CustomID {
		return err
	}

	// write leading html tags
	for i := len(h.Tags) - 1; i >= 0; i-- {
		_, er := fmt.Fprintf(w, "</%v>", h.Tags[0])
		err = errors.Append(err, er)
	}

	return err
}

// CSS Classes for the default element types
const (
	CSSClassUnknownCustom    = `gem-unknown-element`
	CSSClassText             = `gem-text`
	CSSClassLink             = `gem-link`
	CSSClassLinkRocket       = `gem-link-rocket`
	CSSClassPreformattedText = `gem-preformatted-text`
	CSSClass1stHeader        = `gem-header-1`
	CSSClass2ndHeader        = `gem-header-2`
	CSSClass3rdHeader        = `gem-header-3`
	CSSClassUnorderedList    = `gem-list-unordered`
	CSSClassQuote            = `gem-quote`
)

var LineText = HTMLElement{
	Tags:         []string{"p"},
	Class:        CSSClassText,
	JoinAdjacent: true,
	Linebreak:    true,
}

var LinePreformattedText = HTMLElement{
	Tags:      []string{"pre"},
	Class:     CSSClassPreformattedText,
	Linebreak: true,
	MetaWriter: func(tag string, metas map[gemtext.MetaType]string) string {
		if metas[gemtext.PreformattedTextAlttext] == "" {
			return ""
		}
		return "title='" + metas[gemtext.PreformattedTextAlttext] + "'"
	},
}

var LineLink = HTMLElement{
	Tags:   []string{"a"},
	Class:  CSSClassLink,
	Prefix: "<b class='" + CSSClassLinkRocket + "'>=> </b>",
	Suffix: "<br>",
	MetaWriter: func(tag string, metas map[gemtext.MetaType]string) string {
		return "href='" + metas[gemtext.LinkRef] + "'"
	},
}

var LineHeading1 = HTMLElement{
	Tags:  []string{"h1"},
	Class: CSSClass1stHeader,
}
var LineHeading2 = HTMLElement{
	Tags:  []string{"h2"},
	Class: CSSClass2ndHeader,
}
var LineHeading3 = HTMLElement{
	Tags:  []string{"h3"},
	Class: CSSClass3rdHeader,
}

var LineUnorderedList = HTMLElement{
	Tags:         []string{"ul"},
	Class:        CSSClassUnorderedList,
	Linebreak:    true,
	JoinAdjacent: true,
	AlwaysTags:   []string{"li"},
}

var LineQuote = HTMLElement{
	Tags:         []string{"blockquote"},
	Class:        CSSClassQuote,
	Linebreak:    true,
	JoinAdjacent: true,
}
