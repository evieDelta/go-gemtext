# go-gemtext
an implementation of a gemtext parser and formatter with support for extensions.

Documentation is not fully finished and the code hasn't been fully tested, but it should work

## Parser
Included is the ability to parse all standard gemtext and the ability to be expanded with logic for additional linetypes for use as a markup language in web applications

### Examples

#### A custom linetype parser
```go
type Custom struct{}

func (Custom) Config() gemtext.LineConfig {
	return gemtext.LineConfig{
		CustomKey:           "custom",
		Prefix:              "::",
	}
}

func (EndOfLine) Parse(m gemtext.ParseMeta) (gemtext.Line, error) {
	return gemtext.Line{
		Text: m.Text,
	}, nil
}
```

## HTMLRender
In `/htmlrender` is a formatter to turn the parsed gemtext into html, also with support for extensions

### Examples

#### Custom Linetype formatter
```go
type CustomHTML struct {}

func (h CustomHTML) Write(w io.Writer, l gemtext.Line, r LineReader) error {
	_, err := fmt.Fprintf("<p>%v</p>", l.Text)
	return err
}
```

## GemRender
In `/gemrender` is a formatter to turn parsed gemtext with custom line types back into standard gemtext, such as if you are using gemtext as a markup language in a blog distributed to both html and gemtext, and you say define a custom linetype to insert images, naturally you'd render that differently for the gemtext and html versions

In html you'd just insert it directly via an image tag
```html
...
<i src='image.png'>
```
And in gemtext you'd render it as a link to the image file
```gemtext
...
=> gemini://domain/image.png (image.png)
```
Or perhaps a link to a meta page about the image
```gemtext
...
=> gemini://domain/media/image
```
Containing links to the image in multiple formats
```gemtext
# Media / Image
an image of an old computer
=> gemini://domain/files/image.png
=> gemini://domain/files/image.jpg
```

### Examples

#### Custom Linetype formatter
```go
type CustomGem struct {}

func (h CustomGem) Write(w io.Writer, l gemtext.Line, r LineReader) error {
	_, err := fmt.Fprintf("```\n%v\n```", l.Text)
	return err
}
```