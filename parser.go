package gemtext

import (
	"sort"
	"strings"
)

// possible TODO investigate the possibility of reworking this to use bufio
// instead of strings.Index and strings.Split? advantage would be being able to
// parse from a reader instead of requiring the full string up front

// NewParser initialises a new parser
func NewParser(p []LineParser, _ *ParserConfig) *Parser {
	sort.Slice(p, func(i, j int) bool {
		return len(p[i].Config().Prefix) > len(p[j].Config().Prefix)
	})
	return &Parser{
		LineTypes: p,
	}
}

// MinimalParser is a parser with only the basic types
var MinimalParser = NewParser(BasicTypes(), nil)

// DefaultParser is a parser with all the standard gemini types
var DefaultParser = NewParser(AllTypes(), nil)

// Parser is a gemtext parser that outputs an AST
type Parser struct {
	LineTypes []LineParser
}

// ParserConfig ... currently unused reserved for future expansion ...
type ParserConfig struct{}

// Parse a gemtext document into an abstract syntax array
// wrapped with some additional methods for easy reading
//
// under default circumstances error will always be nil
// error is only ever returned if a custom has an error
//
// (bug) be aware as a side effect it currently converts CRLF documents to LF
func (p *Parser) Parse(s string) (LineReader, error) {
	// this is not really intended behavior so it can be treated as a bug
	// as it is an unnecessary side effect and i'd prefer it not do this
	// but i don't feel like dealing with CRLF properly right now
	if strings.Contains(s, "\r\n") {
		// so to prevent CRLF input from causing any weirdness in the output
		// we convert CRLF input to LF, as most programs work with LF too
		// and gemini is required to accept LF in addition to CRLF
		// CR on its own is uncommon and isn't allowed in gemini
		// so i don't think its necessary to workaround too
		s = strings.ReplaceAll(s, "\r\n", "\n")
	}
	// i leave this comment and the note in the doc comment for clear
	// reference to the existence of this shortcoming
	// if anyone wants to put in the work to fix CRLF support feel free to do so

	var lines = make([]Line, 0)
	linecount := strings.Count(s, "\n")

	var err error
	for len(s) > 0 {
		var l Line

		s, l, err = p.readLine(s)
		if err != nil {
			switch b := err.(type) {
			default:
				err = NewError(l.Line, b, "unspecified parser error")
			case ErrorLineModify:
				err = b.ShiftLineNumber(l.Line)
			case ErrorLine:
				// do nothing in this case
			}
			return LineReader{}, err
		}
		// fmt.Println(len(lines), ",", linecount, strings.Count(s, "\n"), l.Line)

		l.Line = (linecount - strings.Count(s, "\n")) - l.Line

		lines = append(lines, l)
	}

	return LineReader{
		Lines: lines,
	}, nil
}

// BUG (eviedelta) crlf is not properly supported, as a work around the Parser automatically converts documents containing crlf to lf only

// no idea what to name this
func (p *Parser) readLine(s string) (string, Line, error) {
	for _, x := range p.LineTypes {
		c := x.Config()
		if !(strings.HasPrefix(s, c.Prefix) && strings.Contains(s[len(c.Prefix):], c.Suffix)) {
			continue
		}
		if !c.Multiline {
			return p.handleSingleline(s, x)
		}

		t, l, ok, err := p.handleMultiline(s, x)
		if !ok {
			continue
		}
		return t, l, err
	}

	return p.handleNomatch(s)
}

func (p *Parser) handleNomatch(s string) (string, Line, error) {
	// if the matcher doesn't find any line types that match then we are just a text line
	b := strings.SplitN(s, "\n", 2)
	l := Line{
		Type:   LineTypeText,
		Text:   b[0],
		Source: b[0],
		Line:   0,
	}

	if len(b) == 1 {
		// if there are no more lines left then we are done
		return "", l, nil
	}
	return b[1], l, nil
}

func (p *Parser) handleSingleline(s string, x LineParser) (string, Line, error) {
	//c := x.Config()

	i := strings.Index(s, "\n")
	//if c.Suffix != "" {
	//	sf := strings.Index(s, c.Suffix)
	//	if sf != -1 && (sf < i || i == -1) && sf+len(c.Suffix) != i {
	//		return p.handleSinglelineWithSuffix(s, x, sf, i)
	//	}
	//}

	if i == -1 {
		i = len(s)
	}

	return p.handleSingleLineWithoutSuffix(s, x, i)
}

func (p *Parser) handleSinglelineWithSuffix(s string, x LineParser, sf, lb int) (string, Line, error) {
	c := x.Config()

	blk := s[len(c.Prefix):sf]
	rem := s[sf:]
	if sf+len(c.Suffix) == lb {
		rem = rem[1:] // remove linebreak
	}

	if c.TrimWhitespace {
		blk = strings.TrimSpace(blk)
	}

	l, err := x.Parse(ParseMeta{Text: blk, TextOnPrefixLine: true})
	if c.CustomKey != "" {
		l.Type = LineTypeCustom
	}
	if l.Type == LineTypeCustom {
		l.CustomID = c.CustomKey
	}
	l.Source = s[:lb+len(c.Suffix)]
	l.Line = 0

	return rem, l, err
}

func (p *Parser) handleSingleLineWithoutSuffix(s string, x LineParser, lb int) (string, Line, error) {
	c := x.Config()

	// b := strings.SplitN(s, "\n", 2)
	blk := strings.TrimPrefix(s[len(c.Prefix):lb], c.Prefix)
	rem := strings.TrimPrefix(s[lb+len(c.Suffix):], "\n")

	blk = strings.TrimSuffix(blk, c.Suffix)
	if c.TrimWhitespace {
		blk = strings.TrimSpace(blk)
	}

	l, err := x.Parse(ParseMeta{Text: blk, TextOnPrefixLine: true})
	if c.CustomKey != "" {
		l.Type = LineTypeCustom
	}
	if l.Type == LineTypeCustom {
		l.CustomID = c.CustomKey
	}
	l.Source = s[:lb]
	l.Line = 0

	//if len(b) == 1 {
	//	return "", l, err
	//}
	return rem, l, err
}

func (p *Parser) handleMultiline(s string, x LineParser) (string, Line, bool, error) {
	c := x.Config()
	pfx := len(c.Prefix)
	if s[pfx] == '\n' {
		pfx++
	}
	suffix := c.Suffix

	switch c.MultilineSuffixMode {
	default:
		fallthrough
	case MultilineSuffixAnywhere:
		b := strings.SplitN(s[pfx:], suffix, 2)

		rem := strings.TrimPrefix(b[1], "\n")
		blk := strings.TrimSuffix(b[0], "\n")

		if c.TrimWhitespace {
			blk = strings.TrimSpace(blk)
		}

		l, err := x.Parse(ParseMeta{Text: blk, TextOnPrefixLine: pfx == len(c.Prefix)})
		if c.CustomKey != "" {
			l.Type = LineTypeCustom
		}
		if l.Type == LineTypeCustom {
			l.CustomID = c.CustomKey
		}
		l.Source = s[:len(b[0])+pfx+len(c.Suffix)]
		l.Line = strings.Count(l.Source, "\n")
		if rem == b[1] {
			l.Line--
			// if the suffix isn't on a newline
			// (means the line that create rem didn't trim a linebreak)
			// then we bump this a notch to correct the line number
		}

		return rem, l, true, err

	// most of the logic of exclusive is shared with before newline
	// and is overall a pretty simple addition
	// so we can do it by just putting linebreak before the suffix
	case MultilineSuffixExclusive:
		suffix = "\n" + c.Suffix
		fallthrough

	case MultilineSuffixBeforeNewline:
		// add the length of the prefix and suffix to a length variable
		i := len(c.Suffix) + pfx

		for {
			// we need to find where the suffix is
			y := strings.Index(s[i:], suffix)
			if y < 0 {
				return "", Line{}, false, nil
			}

			i += y + len(suffix)
			// if we are either at the end or are followed by a linebreak we pass
			if i == len(s) || s[i] == '\n' {
				break
			}
			// if not we try again
		}

		// if its not at the end we bump forward by 1 to include the linebreak
		// just to prevent a stray linebreak from being left after it
		exsuf := 0
		if i != len(s) {
			i++
			exsuf++
		}
		rmlb := 0
		// fmt.Println(s[i-len(suffix)-exsuf-1])
		if s[i-len(suffix)-exsuf-1] == '\n' {
			rmlb++
		}

		// the remainder is everything after i
		// the block is whats after prefix and before suffix
		// rem := strings.TrimPrefix(s[i:], "\n")
		rem := s[i:]
		blk := s[pfx : i-len(suffix)-exsuf-rmlb]

		if c.TrimWhitespace {
			blk = strings.TrimSpace(blk)
		}

		l, err := x.Parse(ParseMeta{Text: blk, TextOnPrefixLine: pfx == len(c.Prefix)})
		if c.CustomKey != "" {
			l.Type = LineTypeCustom
		}
		if l.Type == LineTypeCustom {
			l.CustomID = c.CustomKey
		}
		l.Source = s[:i-exsuf]
		l.Line = strings.Count(l.Source, "\n") - 1 + exsuf

		return rem, l, true, err
	}
}
